<head>
    <link rel="stylesheet" href="custom.css">
    <link rel="icon" type="image/png" sizes="16x16" href="#">
    <link rel="manifest" href="/site.webmanifest">
</head>
<body>
    <div class="container">
        <div class="banner">
            <img src="{{ url('assets/slider/slider1.png') }}" alt="">
        </div>

        <section class="pilih-liga mt-4">
            <h3><strong>Pilihan Liga</strong></h3>
            <div class="row mt-4">
                @foreach($ligas as $liga)
                <div class="col">
                    <a href="{{ route('products.liga', $liga->id)}}">
                    <div class="card shadow">
                        <div class="card-body text-center">
                            <img src="{{ url('assets/liga') }}/{{ $liga->gambar }}" class="img-fluid">
                        </div>
                    </div>
                    </a>
                </div>
            @endforeach
            </div>
        </section>
    
        <section class="products mt-5 mb-5">
            <h3>
            <strong>Best Products</strong>
            <a href="{{ route('products') }}" class="btn btn-dark float-right" ><i class="fas fa-eye"></i> Lihat Semua</a>
            </h3>
            <div class="row mt-4">
                @foreach($products as $product)
                <div class="col-md-3">
                    <div class="card shadow">
                        <div class="card-body text-center">
                            <img src="{{ url('assets/jersey') }}/{{ $product->gambar }}" class="img-fluid">
                            <div class="row mt-2 ">
                                <div class="col-md-12">
                                    <h6>{{ $product->nama }}</h6>
                                    <h6> RP. {{ number_format($product->harga) }}</h6>
                                </div>
                            </div>
                            <div class="row mt-2 ">
                                <div class="col-md-12">
                                    <a href="{{ route('products.detail', $product->id) }}" class="btn btn-dark btn-block"><i class="fas fa-eye">  </i>  Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
    
    </div>
</body>
</html>
