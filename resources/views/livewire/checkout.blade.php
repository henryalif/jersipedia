<div class="container">
    <div class="row mt-4 mb-2">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" class="text-dark">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('keranjang') }}" class="text-dark">Keranjang</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-danger">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>

    <div class="row mb-4">
        <div class="col">
            <a class="btn btn-dark" href="{{ route('keranjang') }}" role="button"><i class="fas fa-arrow-left"></i>
                Kembali</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-6">
            <!-- transaksi  -->
            <h3>Informasi Pembayaran</h3>
            <hr>
            <h6 align="center"><strong>CUKUP MEMILIH SALAH SATU CARA PEMBAYARAN</strong></h6>
            <div class="card shadow">
                <div class="card-body">
                    <div class="media">
                        <img class="align-self-center mr-3" src="{{ url('assets/bri.png')}}"
                            alt="Generic placeholder image" width="60px">
                        <div class="media-body">
                            <h4 class="mt-0"><strong>Transfer Bank BRI</strong></h4>
                            <p>Nominal yang harus di transfer Sebesar <strong> Rp.
                                    {{ number_format($total_harga) }}</strong> </p>
                            <hr>
                            No. Rekening : <strong>0821-2400-8935</strong><br>
                            Atas Nama : <strong>Henry Alif Rahmadhana</strong>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="card shadow">
                <div class="card-body">
                    <div class="media">
                        <img class="align-self-center mr-3" src="{{ url('assets/sopi.png')}}"
                            alt="Generic placeholder image" width="60px">
                        <div class="media-body">
                            <h4 class="mt-0"><strong>Shopee</strong></h4>
                            <hr>
                            Nama Toko : <strong>JerseyPedia.id</strong><br><br>
                            <a class="btn btn-success btn-block" href="{{ url('https://shopee.co.id/') }}"
                                role="button"><i class="fas fa-search"></i> Klik untuk meluncur ke Shopee</a>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="card shadow">
                <div class="card-body">
                    <div class="media">
                        <img class="align-self-center mr-3" src="{{ url('assets/tokped.png')}}"
                            alt="Generic placeholder image" width="60px">
                        <div class="media-body">
                            <h4 class="mt-0"><strong>TokoPedia</strong></h4>
                            <hr>
                            Nama Toko : <strong>JerseyPedia.id</strong><br><br>
                            <a class="btn btn-success btn-block" href="{{ url('https://www.tokopedia.com/') }}"
                                role="button"><i class="fas fa-search"></i> Klik untuk meluncur ke TokoPedia</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- pengiriman -->
        <div class="col-md-6">
            <h3>Informasi Pengiriman</h3>
            <hr>
            <form wire:submit.prevent="checkout">
                <h6 align="center"><strong>LENGKAPI FORMULIR DIBAWAH INI :</strong></h6>
                <div class="card shadow">
                    <div class="card-body">

                        <H4>*Bila Transaksi Melalui Transfer Bank*</H4>
                        <div class="form-group">
                            <label for="">No. HP</label>
                            <input id="no_hp" type="text" class="form-control @error('no_hp') is-invalid @enderror"
                                wire:model="no_hp" value="{{ old('no_hp') }}" autocomplete="name" autofocus>

                            @error('nohp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="">Alamat</label>
                            <textarea wire:model="alamat"
                                class="form-control @error('nama') is-invalid @enderror"></textarea>

                            @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for=""><strong>Upload Bukti Transfer</strong></label>
                            <input class="form-control @error('transfer') is-invalid @enderror" type="file"
                                id="formFile" wire:model="transfer">
                        </div>
                        <button type="submit" class="btn btn-success btn-block" href="{{ route('history') }}"><i
                                class=" fas fa-coins"></i> Checkout </button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
