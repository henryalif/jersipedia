<div class="container">
    <div class="row mt-4 mb-2">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" class="text-dark">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">History</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>


    <div class="row mt-3">
        <div class="col">
            <div class="table-responsive">
                <table class="table text-center table-hover table-light table-striped shadow">
                    <thead class="table-secondary">
                        <tr class="judulnya">
                            <td>No.</td>
                            <td>Tanggal Pesan</td>
                            <td>Order Number</td>
                            <td>Pesanan</td>
                            <td><strong>Total Harga</strong></td>
                            <td>Invoice</td>
                            <td>Diterima</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1 ?>
                        @forelse ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $pesanan->created_at }}</td>
                            <td>{{ $pesanan->kode_pemesanan }}</td>
                            <td>
                                <?php 
                                        $pesanan_detail = App\Models\PesananDetail::where('pesanan_id', $pesanan->id)->get(); ?>
                                @foreach ($pesanan_detail as $pesanan_detail)
                                <img src="{{ url('assets/jersey') }}/{{ $pesanan_detail->product->gambar }}"
                                    class="img-fluid" width="60">
                                {{ $pesanan_detail->product->nama}}
                                @endforeach
                            </td>
                            <td><strong>{{ number_format($pesanan->total_harga) }}</strong></td>
                            <td>
                                <button type="button" class="btn btn-warning">Cetak</button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info"><i class="fas fa-check"></i></button>
                            </td>
                        </tr>

                        @empty
                        <tr>
                            <td colspan="9">
                                <h4><strong>Tidak Ada Transaksi</strong></h4>
                            </td>
                        </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <div class="media">
                        <img class="align-self-center mr-3" src="{{ url('assets/bri.png')}}"
                            alt="Generic placeholder image" width="60px">
                        <div class="media-body">
                            <p>Untuk transaksi, silahkan transfer ke rekening berikut :</p>
                            <hr>
                            No. Rekening : <strong>0821-2400-8935</strong><br>
                            Atas Nama : <strong>Henry Alif Rahmadhana</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
