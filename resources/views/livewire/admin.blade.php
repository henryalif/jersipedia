<div class="container">
    <div class="row mt-4 mb-2">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" class="text-dark">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Admin</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <h1>DATA PEMESANAN</h1>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col">
            <div class="table-responsive">
                <table class="table text-center table-hover table-light table-striped shadow">
                    <thead class="table-secondary">
                        <tr class="judulnya">
                            <td>No.</td>
                            <td>User</td>
                            <td>Order Date</td>
                            <td>Kode</td>
                            <td>Order</td>
                            <td>Status</td>
                            <td><strong>Total Harga</strong></td>
                            <td>Bukti TF</td>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 1 ?>
                        @forelse ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><button type="button" class="btn btn-success">Lunas</button></td>
                            <td></td>
                            <td><button type="button" class="btn btn-info">Bukti</button></td>
                        </tr>

                        @empty
                        <tr>
                            <td colspan="9">
                                <h4><strong>Tidak Ada Transaksi</strong></h4>
                            </td>
                        </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
