<div class="container">
    <div class="row mt-4 mb-2">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" class="text-dark">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('products') }}" class="text-dark">List Jersey</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Jersey Detail</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card gambar-product shadow">
                <div class="card-body">
                    <img src="{{ url('assets/jersey') }}/{{ $product->gambar }}" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h2>
                <strong>{{ $product->nama }}</strong><br>
                @if($product->is_ready == 1)
                <span class="badge rounded-pill badge-success"> <i class="fas fa-check"></i> Ready Stok</span>
                @else
                <span class="badge rounded-pill badge-danger"> <i class="fas fa-times"></i> Stok Habis</span>
                @endif
            </h2>
            <h4>
                Rp. {{ number_format($product->harga) }}
            </h4>

            <div class="row">
                <div class="col">
                    <form wire:submit.prevent="masukkanKeranjang"> 
                    <table class="table" style="border-top : hidden">
                        <tr>
                            <td>Kategori Liga</td>
                            <td>:</td>
                            <td><strong>{{ $product->liga->nama }}</strong></td>
                        </tr>
                        <tr>
                            <td>Jenis Jersey</td>
                            <td>:</td>
                            <td><strong>{{ $product->jenis }}</strong></td>
                        </tr>
                        <tr>
                            <td>Berat</td>
                            <td>:</td>
                            <td><strong>{{ $product->berat }} Kg</strong></td>
                        </tr>
                        <tr>
                            <td>Jumlah Barang</td>
                            <td>:</td>
                            <td>
                                <input id="jumlah_pesanan" type="number"
                                    class="form-control @error('jumlah_pesanan') is-invalid @enderror"
                                    wire:model="jumlah_pesanan" value="{{ old('jumlah_pesanan') }}" required
                                    autocomplete="name" autofocus>

                                @error('jumlah_pesanan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        @if($jumlah_pesanan > 1)
                        @else
                        <tr>
                            <td colspan="3"><strong>Name Set (Isi bila menggunakan Name Set)</strong> </td>
                        </tr>
                        <tr>
                            <td>Harga Name Set</td>
                            <td>:</td>
                            <td><strong>Rp. {{ number_format($product->harga_nameset) }}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama Punggung</td>
                            <td>:</td>
                            <td>
                                <input id="nama" type="text"
                                    class="form-control @error('nama') is-invalid @enderror"
                                    wire:model="nama" value="{{ old('nama') }}"
                                    autocomplete="name" autofocus>

                                @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor Punggung</td>
                            <td>:</td>
                            <td>
                                <input id="nomer" type="number"
                                    class="form-control @error('nomer') is-invalid @enderror"
                                    wire:model="nomer" value="{{ old('nomer') }}"
                                    autocomplete="name" autofocus>

                                @error('nomer')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td colspan="3">
                                <button type="submit" class="btn btn-dark btn-block" @if($product->is_ready !== 1) disabled @endif><i class="fas fa-shopping-cart"></i>  Masukkan Keranjang</button>
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>