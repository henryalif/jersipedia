<?php

namespace App\Http\Livewire;

use App\Models\Pesanan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Checkout extends Component
{
    public $total_harga, $no_hp, $alamat, $transfer;

    use WithFileUploads; //buat upload foto/image

    public function mount()
    {
        if(!Auth::user()) {
            return redirect()->route('login');
        }

        $this->no_hp = Auth::user()->no_hp;
        $this->alamat = Auth::user()->alamat;

        $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

        if(!empty($pesanan))
        {
            $this->total_harga = $pesanan->total_harga+$pesanan->kode_unik;
        }else {
            return redirect()->route('home');
        }
    }

    public function checkout()
    {
        $this->validate([
            'no_hp' => 'required',
            'alamat' => 'required',
            'transfer' => 'image' //ini buat image
        ]);

        //Simpan nohp Alamat ke data user
        $user = User::where('id', Auth::user()->id)->first();
        $user->no_hp = $this->no_hp;
        $user->alamat = $this->alamat;
        $user->update();


        //update data pesanan
        $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();
        $pesanan->status = 1;
        $pesanan->update();

        $this->transfer->store('photos'); //ini buat image

        $this->emit('masukKeranjang');

        session()->flash('message', "Berhasil Checkout");

        return redirect()->route('history');
    }

    public function render()
    {
        return view('livewire.checkout');
    }
}
