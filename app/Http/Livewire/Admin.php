<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Pesanan;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class Admin extends Component
{
    public $pesanans, $users;
    public function render()
    {
        if(Auth::user())
        {
            $this->pesanans = Pesanan::where('user_id', Auth::user()->id)->where('status','!=', 0)->get();
        }

        if(Auth::user())
        {
        $this->users = User::where('name', Auth::user())->get();
        }
        return view('livewire.admin');
    }
}
