<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Liga;

use Livewire\Component;

class Home extends Component
{
    public function render()
    {
        return view('livewire.home', [
            'products' => product::take(4)->get(),
            'ligas' => liga::all()
        ]);
    }
}
