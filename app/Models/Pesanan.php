<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{

    protected $fillable = [
        'kode_pemesanan',
        'status',
        'total_harga',
        'kode_unik',
        'user_id',
    ];

    public function pesanan_details()
    {
        return $this->hasMany(Pesanan_details::class,'pesanan_id','id');
    }

    public function user()
    {
    return $this->belongsTo(User::class, 'User_id', 'id');
    }
}
